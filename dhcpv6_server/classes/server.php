<?php

require_once "packet.php";
require_once "constants.php";
require_once "requestProcessor.php";
require_once "storage.php";

class dhcpv6Server {
	private $packetProcessor = null;
	private $socket = null;
	private $storage = null;
	public $verbosity;
	private $broadcast_addr;
	private $_port;


	function __construct($port=DHCPV6_PORT, $verbosity = 1)
	{
		$this->verbosity = $verbosity;
		$this->storage = new dhcpv6Storage();
		$this->_port=$port;

		$this->socket = socket_create(AF_INET6, SOCK_DGRAM, SOL_UDP);
		$resp=socket_bind($this->socket, DHCPV6_BIND_ADDRESS, $this->_port);
		
		socket_set_option($this->socket, IPPROTO_IPV6, MCAST_JOIN_GROUP, array("group" => IPV6_MULTICAST_RELAYS_AND_SERVERS, "interface" => DHCPV6_INTERFACE));
		socket_set_option($this->socket, IPPROTO_IPV6, MCAST_JOIN_GROUP, array("group" => IPV6_MULTICAST_SERVERS_ONLY, "interface" => DHCPV6_INTERFACE));	
	}


	function listen() {
		while(1) {
			$this->verbosity && print('Listening -----------------------------------------------------------' . "\n");
			$peer_address="";
						
			socket_recvfrom($this->socket, $data, 4608, MSG_OOB, $peer_address, $peer_port); 

			$this->processPacket($data,$peer_address,$peer_port);

			$this->verbosity && print("\n\n");
		}
	}

	
	function processPacket($packetData,$peer_address,$peer_port) {
		
		$packet=new dhcpv6Packet();
		$packet->parse($packetData);

		$processor= new dhcpv6RequestProcessor($this, $this->storage, $packet);

		$send_data=$processor->getResponse();

		socket_sendto($this->socket, $send_data, strlen($send_data), MSG_OOB, $peer_address, $peer_port);		
	}
}

function dhcpv6_print_message($strMessage, $hOutput=null)
{
	static $hOutputStream;
	if($hOutput!=null)
		$hOutputStream = $hOutput;
	fwrite($hOutputStream, $strMessage."\n"); 
}
