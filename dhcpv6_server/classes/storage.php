<?php
require_once 'packet.php';

class dhcpv6Storage
{

	function get_mac_from_DUID($strDUID)
	{
		$nDUIDType=intval(substr($strDUID,0,4));
		if ($nDUIDType == 1)
			$strMAC=substr($strDUID,16);
		else 
		if ($nDUIDType == 3)
			$strMAC=substr($strDUID,8);
		else $strMAC="";

		$arrMACPieces=str_split($strMAC,2);
		$strMACWithDots=implode(":",$arrMACPieces);

		return $strMACWithDots;
	}

	function allocateIP($IAID, $client, $wantedAddr)
	{
		if ($IAID == "7ea9f217" || $IAID == "10bc3529")
			return "30010001000000000000000000000004";
		else
			return "3001000100000000000000000000000a";
	}


	function checkIAlink($IAID, $client, $option)
	{
		return $this->checkIAbinding($IAID, $client, $option);
	}

	function checkIAbinding($IAID, $client, $option)
	{
		if ($IAID == "7ea9f217" || $IAID == "10bc3529")
			return true;
		return false;
	}
	
	function releaseIAbinding($IAID, $client, $option, $alreadyUsed)
	{
		return true;
	}
}

