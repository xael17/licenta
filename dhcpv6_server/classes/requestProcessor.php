<?php
	
require_once "storage.php";
require_once "packet.php";

class dhcpv6RequestProcessor {
	private $packet = null;
	private $response = null;
	private $storage = null;
	
	function handleIAADDR($IAID, $client, $option=null, $available=true) 
	{
		if ($option==null)
		{
			$option=array();			
			$option["type"]=OPT_TYPE_IA_ADDR;
			$option["data"]=NULL_IPV6_ADDR;
			$option["data"].= NULL_TIME_CONST; 
			$option["data"].= NULL_TIME_CONST; 
			$option["len"]=strlen($option["data"])/2;
		}
		$wantedAddr=substr($option["data"], IPV6_STR_SIZE);
		
		$allocatedIP=$this->storage->allocateIP($IAID, $client, $wantedAddr);
		$option["data"]=$allocatedIP;
		if ($available)
		{
			$option["data"].=IAADDR_PREFERRED_TIME; 
			$option["data"].=IAADDR_VALID_TIME;
		}
		else
			$option["data"].= NULL_TIME_CONST.NULL_TIME_CONST; 
		return $option;
	}

	function handleIANA($option, $client, $status=STATUS_SUCCESS)
	{
		$responseOption=array();
		$responseOption["type"]=OPT_TYPE_IA_NA;
		
		$IAID=substr($option["data"], 0, IAID_STR_SIZE);
		$responseOption["data"]=$IAID;
		$responseOption["data"].=IANA_T1_TIME; 
		$responseOption["data"].=IANA_T2_TIME; 
	
		if ($status == STATUS_SUCCESS)
		{
			if ($option["len"] == IANA_OPTION_NO_IADDR_LEN)
			{
				$IAOption=$this->handleIAADDR($IAID,$client);		
				$unpackedResponse=unpack("H*IA", $this->buildResponseFromOption($IAOption));	
				$responseOption["data"] .= $unpackedResponse["IA"];	
			}
			else
			{
				$IAOptions = $this->packet->parseOptions(substr($option["data"], IANA_OPTION_NO_IADDR_LEN * 2));
				foreach($IAOptions as $IAOption)
				{
					$IAResponse=$this->handleIAADDR($IAID, $client, $IAOption);
					$unpackedResponse=unpack("H*IA", $this->buildResponseFromOption($IAResponse));
					$responseOption["data"] .= $unpackedResponse["IA"];
				}			
			}
		}
		else if ($status == STATUS_NO_BINDING)
		{
			$objOptionStatusNoBinding=array
			(
				"type"=>OPT_TYPE_STATUS,
				"len"=>strlen(STATUS_NO_BINDING)/2,
				"data"=>STATUS_NO_BINDING
			);
			$unpackedResponse=unpack("H*St", $this->buildResponseFromOption($objOptionStatusNoBinding));
			$responseOption["data"] .=$unpackedResponse["St"];
		}
		else if ($status == STATUS_NOT_ON_LINK)
		{
			$IAOptions = $this->packet->parseOptions(substr($option["data"], IANA_OPTION_NO_IADDR_LEN * 2));
			foreach($IAOptions as $IAOption)
			{
				$IAResponse=$this->handleIAADDR($IAID, $client, $IAOption, false);
				$unpackedResponse=unpack("H*IA", $this->buildResponseFromOption($IAResponse));
				$responseOption["data"] .= $unpackedResponse["IA"];
			}	
		}
		
		$responseOption["len"]=strlen($responseOption["data"])/2;
		return $responseOption;
	}

	function checkIANAlink($option,$client)
	{
		if ($option["len"] == IANA_OPTION_NO_IADDR_LEN) return false;
		
		$IAID=substr($option["data"], 0, IAID_STR_SIZE);		
		$IAOptions = $this->packet->parseOptions(substr($option["data"], IANA_OPTION_NO_IADDR_LEN*2));

			
		foreach($IAOptions as $IAOption)
		{
			$IAStatus = $this->storage->checkIAlink($IAID, $client, $IAOption);
			if (!$IAStatus) return false;
		}

		return true;
	}

	function checkIANAbinding($option, $client)
	{
		if ($option["len"] == IANA_OPTION_NO_IADDR_LEN) return false;
		
		$IAID=substr($option["data"], 0, IAID_STR_SIZE);		
		$IAOptions = $this->packet->parseOptions(substr($option["data"], IANA_OPTION_NO_IADDR_LEN*2));
		
		foreach($IAOptions as $IAOption)
		{
			$IAStatus = $this->storage->checkIAbinding($IAID, $client, $IAOption);
			if (!$IAStatus) return false;
		}
		
		return true;
	}

	function releaseIANAbinding($option, $client, $alreadyUsed = false)
	{
		if ($option["len"] == IANA_OPTION_NO_IADDR_LEN) return false;
		
		$IAID=substr($option["data"], 0, IAID_STR_SIZE);		
		$IAOptions = $this->packet->parseOptions(substr($option["data"], IANA_OPTION_NO_IADDR_LEN*2));

		$status=true;
		
		foreach($IAOptions as $IAOption)
		{
			$IAStatus = $this->storage->releaseIAbinding($IAID, $client, $IAOption, $alreadyUsed);
			$status = $status && $IAStatus;
		}
		return $status;
	}

	function handleOptionRequest($option, &$responsePackage)
	{
		$nrReq = $option["len"]/2;
		for($i=0; $i<$nrReq; $i++)
		{
			$optReq = hexdec(substr($option["data"], 4*$i, 4));
			switch($optReq)
			{
				// until we have actual values for these, don't treat them				

				/*case OPT_TYPE_DNS_RNS:
					$responsePackage->packetData["options"][]=array
					(
						"type"=>OPT_TYPE_DNS_RNS,
						"len"=>strlen(DNS_RNS)/2,
						"data"=>DNS_RNS
					);
					break;

				case OPT_TYPE_DNS_SEARCHLIST:
					$responsePackage->packetData["options"][]=array
					(
						"type"=>OPT_TYPE_DNS_SEARCHLIST,
						"len"=>strlen(DNS_SEARCHLIST)/2,
						"data"=>DNS_SEARCHLIST
					);
					break;

				case OPT_TYPE_SNTP_SERVER:
					$responsePackage->packetData["options"][]=array
					(
						"type"=>OPT_TYPE_SNTP_SERVER,
						"len"=>strlen(SNTP_SERVER)/2,
						"data"=>SNTP_SERVER
					);
					break;*/
				case 59:
					$strIP = bin2hex("http://[3001:0001:0000:0000:0000:0000:0000:0001]/");
					$responsePackage->packetData["options"][]=array
					(
						"type"=>59,
						"len"=>strlen($strIP)/2,
						"data"=>$strIP
					);
					break;
					
				default:
					break;
			}
		}
	}

	function buildResponseFromOption($option)
	{
		$response="";
		$typeHex = dechex($option["type"]);
		$typeHex = substr("0000",0,4-strlen($typeHex)).$typeHex;
		$response .= pack("H*",$typeHex);
		
		$lenHex = dechex($option["len"]);
		$lenHex = substr("0000",0,4-strlen($lenHex)).$lenHex;
		$response .= pack("H*",$lenHex);
			
		$response .= pack("H*",$option["data"]);
	
		return $response;
	}

	function buildResponse($packet)
	{
		$typeHex = dechex($packet->packetData["type"]);
		$typeHex = substr("00",0,2-strlen($typeHex)).$typeHex;
		$response = pack("H*",$typeHex);
		$response .= pack("H*",$packet->packetData["transaction"]);

		foreach($packet->packetData["options"] as $option)
			$response .= $this->buildResponseFromOption($option);	

		return $response;
	}

	function buildRelayResponse($packet)
	{
		$typeHex = dechex($packet->packetData["type"]);
		$typeHex = substr("00",0,2-strlen($typeHex)).$typeHex;
		$response = pack("H*",$typeHex);
		$hopHex = dechex($packet->packetData["hop"]);
		$hopHex = substr("00",0,2-strlen($hopHex)).$hopHex;
		$response .= pack("H*",$hopHex);
		$response .= pack("H*",$packet->packetData["client"]);
		$response .= pack("H*",$packet->packetData["source"]);

		foreach($packet->packetData["options"] as $option)
			$response .= $this->buildResponseFromOption($option);
		
		return $response;
	}

	function handleClientSolicit($packet, &$response)
	{
		$responsePackage = new dhcpv6Packet;
		$responsePackage->packetData["type"]=MSG_TYPE_ADVERTISE;
		$responsePackage->packetData["transaction"]=$packet->packetData["transaction"];

		$responsePackage->packetData["options"]=array();
		
		$responsePackage->packetData["options"][]=array
		(
			"type"=>OPT_TYPE_SRV_ID,
			"len"=>strlen(SERVER_DUID)/2,
			"data"=>SERVER_DUID
		);

		$responsePackage->packetData["options"][]=array
		(
			"type" => OPT_TYPE_PREF,
			"len" => strlen(SERVER_PREFERENCE)/2,
			"data" => SERVER_PREFERENCE
		);
		
		$clientID="";
		foreach($packet->packetData["options"] as $option)
		{
			if ($option["type"] == OPT_TYPE_CL_ID)
			{
				$responsePackage->packetData["options"][]=$option;
				$clientID=$option["data"];			
			}		
		}

		if ($clientID == "")
		{
			$response=null;
			return null;
		}

		foreach($packet->packetData["options"] as $option)
		{
			if ($option["type"] == OPT_TYPE_IA_NA)
			{
				$responsePackage->packetData["options"][]=$this->handleIANA($option, $clientID);
			}
			else if ($option["type"] == OPT_TYPE_OPT_REQ)
			{
				$this->handleOptionRequest($option, $responsePackage);
			}
								
		}		
		
		$response = $this->buildResponse($responsePackage);
	}

	function handleClientRequest($packet,&$response)
	{
		$responsePackage = new dhcpv6Packet;
		$responsePackage->packetData["type"]=MSG_TYPE_REPLY;
		$responsePackage->packetData["transaction"]=$packet->packetData["transaction"];

		$responsePackage->packetData["options"]=array();
		
		$clientID="";
		$serverID="";
		foreach($packet->packetData["options"] as $option)
		{
			if ($option["type"] == OPT_TYPE_CL_ID)
			{
				$responsePackage->packetData["options"][]=$option;
				$clientID=$option["data"];			
			}
			else if ($option["type"] == OPT_TYPE_SRV_ID)
			{
				if ($option["data"] != SERVER_DUID)
				{
					$response = null;					
					return null;
				}
				else
				{
					$responsePackage->packetData["options"][]=$option;
					$serverID=$option["data"];
				}
			}
		}

		if ($clientID=="" || $serverID=="")
		{
			$response = null;
			return null;
		}

		foreach($packet->packetData["options"] as $option)
		{
			if ($option["type"] == OPT_TYPE_IA_NA)
			{
				$responsePackage->packetData["options"][]=$this->handleIANA($option, $clientID);
			}
			else if ($option["type"] == OPT_TYPE_OPT_REQ)
			{
				$this->handleOptionRequest($option, $responsePackage);
			}					
		} 

		$response = $this->buildResponse($responsePackage);
	}

	function handleClientConfirm($packet, &$response)
	{
		$responsePackage = new dhcpv6Packet;
		$responsePackage->packetData["type"]=MSG_TYPE_REPLY;
		$responsePackage->packetData["transaction"]=$packet->packetData["transaction"];

		$responsePackage->packetData["options"]=array();
		
		$responsePackage->packetData["options"][]=array
		(
			"type"=>OPT_TYPE_SRV_ID,
			"len"=>strlen(SERVER_DUID)/2,
			"data"=>SERVER_DUID
		);

		$clientID="";
		foreach($packet->packetData["options"] as $option)
		{
			if ($option["type"] == OPT_TYPE_CL_ID)
			{
				$responsePackage->packetData["options"][]=$option;
				$clientID=$option["data"];			
			}
			else if ($option["type"] ==  OPT_TYPE_SRV_ID)
			{
				$response=null;
				return null;
			}
		}

		if ($clientID == "")
		{
			$response=null;
			return null;
		}

		$confirm = true;

		foreach($packet->packetData["options"] as $option)
		{
			if ($option["type"] == OPT_TYPE_IA_NA)
			{
				if ($option["len"] == IANA_OPTION_NO_IADDR_LEN)
				{
					$response=null;
					return null;
				}
				else
				{
					$confirm=$confirm && $this->checkIANAlink($option, $clientID);
				}	
			}		
		}
		
		if ($confirm)
		{
			$responsePackage->packetData["options"][]=array
			(
				"type"=>OPT_TYPE_STATUS,
				"len"=>strlen(STATUS_SUCCESS)/2,
				"data"=>STATUS_SUCCESS
			);
		}
		else
		{
			$responsePackage->packetData["options"][]=array
			(
				"type"=>OPT_TYPE_STATUS,
				"len"=>strlen(STATUS_NOT_ON_LINK)/2,
				"data"=>STATUS_NOT_ON_LINK
			);
		}

		$response = $this->buildResponse($responsePackage);
	}

	function handleClientRenew($packet, &$response)
	{
		$responsePackage = new dhcpv6Packet;
		$responsePackage->packetData["type"]=MSG_TYPE_REPLY;
		$responsePackage->packetData["transaction"]=$packet->packetData["transaction"];

		$responsePackage->packetData["options"]=array();
		
		$clientID="";
		$serverID="";
		foreach($packet->packetData["options"] as $option)
		{
			if ($option["type"] == OPT_TYPE_CL_ID)
			{
				$responsePackage->packetData["options"][]=$option;
				$clientID=$option["data"];			
			}
			else if ($option["type"] == OPT_TYPE_SRV_ID)
			{
				if ($option["data"] != SERVER_DUID)
				{
					$response = null;					
					return null;
				}
				else
				{
					$responsePackage->packetData["options"][]=$option;
					$serverID=$option["data"];
				}
			}
		}

		if ($clientID=="" || $serverID="")
		{
			$response=null;
			return null;
		}

		foreach($packet->packetData["options"] as $option)
		{
			if ($option["type"] == OPT_TYPE_IA_NA)
			{
				$binded=$this->checkIANAbinding($option, $clientID);
				if ($binded)
				{
					$link=$this->checkIANAlink($option, $clientID);
					if ($link)
						$responsePackage->packetData["options"][]=$this->handleIANA($option, $clientID, STATUS_SUCCESS);	
					else
						$responsePackage->packetData["options"][]=$this->handleIANA($option, $clientID, STATUS_NOT_ON_LINK);	
				}
				else
				{
					$responsePackage->packetData["options"][]=$this->handleIANA($option, $clientID, STATUS_NO_BINDING);	
				}
			}
			else if ($option["type"] == OPT_TYPE_OPT_REQ)
			{
				$this->handleOptionRequest($option, $responsePackage);
			}				
		}
		$response = $this->buildResponse($responsePackage);
	}

	function handleClientRebind($packet,&$response)
	{
		$responsePackage = new dhcpv6Packet;
		$responsePackage->packetData["type"]=MSG_TYPE_REPLY;
		$responsePackage->packetData["transaction"]=$packet->packetData["transaction"];

		$responsePackage->packetData["options"]=array();
		
		$responsePackage->packetData["options"][]=array
		(
			"type"=>OPT_TYPE_SRV_ID,
			"len"=>strlen(SERVER_DUID) / 2,
			"data"=>SERVER_DUID
		);

		$clientID="";
		foreach($packet->packetData["options"] as $option)
		{
			if ($option["type"] == OPT_TYPE_CL_ID)
			{
				$responsePackage->packetData["options"][]=$option;
				$clientID=$option["data"];			
			}
			else if ($option["type"] == OPT_TYPE_SRV_ID)
			{
				$response=null;
				return null;
			}
		}
		
		if ($clientID=="")
		{
			$response=null;
			return null;
		}

		foreach($packet->packetData["options"] as $option)
		{
			if ($option["type"] == OPT_TYPE_IA_NA)
			{
				$link=$this->checkIANAlink($option, $clientID);				
				if ($link)
				{
					$binded=$this->checkIANAbinding($option, $clientID);
					if ($binded)
						$responsePackage->packetData["options"][]=$this->handleIANA($option, $clientID, STATUS_SUCCESS);	
					else
					{
						$response=null;
						return null; 
					}
						
				}
				else
				{
					
					$responsePackage->packetData["options"][]=$this->handleIANA($option, $clientID, STATUS_NOT_ON_LINK);	
				}
			}
			else if ($option["type"] == OPT_TYPE_OPT_REQ)
			{
				$this->handleOptionRequest($option, $responsePackage);
			}				
		}
		
		$response = $this->buildResponse($responsePackage);
	}


	function handleClientInfoRequest($packet,&$response)
	{
		$responsePackage = new dhcpv6Packet;
		$responsePackage->packetData["type"]=MSG_TYPE_REPLY;
		$responsePackage->packetData["transaction"]=$packet->packetData["transaction"];

		$responsePackage->packetData["options"]=array();
		
		$responsePackage->packetData["options"][]=array
		(
			"type"=>OPT_TYPE_SRV_ID,
			"len"=>strlen(SERVER_DUID) / 2,
			"data"=>SERVER_DUID
		);

		foreach($packet->packetData["options"] as $option)
		{
			if ($option["type"] == OPT_TYPE_CL_ID)
			{
				$responsePackage->packetData["options"][]=$option;
				$clientID=$option["data"];			
			}
		}

		foreach($packet->packetData["options"] as $option)
		{
			if ($option["type"] == OPT_TYPE_OPT_REQ)
			{
				$this->handleOptionRequest($option, $responsePackage);
			}
			else if ($option["type"] == OPT_TYPE_SRV_ID && $option["data"] != OPT_TYPE_SRV_ID)
			{
				$response=null;
				return null;
			}				
		}
		
		$response = $this->buildResponse($responsePackage);
	}


	function handleClientRelease($packet, &$response)
	{
					
		$responsePackage = new dhcpv6Packet;
		$responsePackage->packetData["type"]=MSG_TYPE_REPLY;
		$responsePackage->packetData["transaction"]=$packet->packetData["transaction"];

		$responsePackage->packetData["options"]=array();
		
		$clientID="";
		$serverID="";
		foreach($packet->packetData["options"] as $option)
		{
			if ($option["type"] == OPT_TYPE_CL_ID)
			{
				$responsePackage->packetData["options"][]=$option;
				$clientID=$option["data"];			
			}
			else if ($option["type"] == OPT_TYPE_SRV_ID)
			{
				if ($option["data"] != SERVER_DUID)
				{
					$response = null;					
					return null;
				}
				else
				{
					$responsePackage->packetData["options"][]=$option;
					$serverID=$option["data"];
				}
			}
		}

		if ($clientID=="" || $serverID="")
		{
			$response=null;
			return null;
		}
	
		foreach($packet->packetData["options"] as $option)
		{
			if ($option["type"] == OPT_TYPE_IA_NA)
			{
				$released=$this->releaseIANABinding($option, $clientID);				
				if (!$released)
					$responsePackage->packetData["options"][]=$this->handleIANA($option, $clientID, STATUS_NO_BINDING);	
			}				
		}

		$responsePackage->packetData["options"][]=array
			(
				"type"=>OPT_TYPE_STATUS,
				"len"=>strlen(STATUS_SUCCESS)/2,
				"data"=>STATUS_SUCCESS
			);


		$response = $this->buildResponse($responsePackage);
	}

	
	function handleClientDecline($packet, &$response)
	{
		$responsePackage = new dhcpv6Packet;
		$responsePackage->packetData["type"]=MSG_TYPE_REPLY;
		$responsePackage->packetData["transaction"]=$packet->packetData["transaction"];

		$responsePackage->packetData["options"]=array();
		
		$clientID="";
		$serverID="";
		foreach($packet->packetData["options"] as $option)
		{
			if ($option["type"] == OPT_TYPE_CL_ID)
			{
				$responsePackage->packetData["options"][]=$option;
				$clientID=$option["data"];			
			}
			else if ($option["type"] == OPT_TYPE_SRV_ID)
			{
				if ($option["data"] != SERVER_DUID)
				{
					$response = null;					
					return null;
				}
				else
				{
					$responsePackage->packetData["options"][]=$option;
					$serverID=$option["data"];
				}
			}
		}

		if ($clientID=="" || $serverID="")
		{
			$response=null;
			return null;
		}
	
		foreach($packet->packetData["options"] as $option)
		{
			if ($option["type"] == OPT_TYPE_IA_NA)
			{
				$released=$this->releaseIANABinding($option, $clientID, true);				
				if (!$released)
					$responsePackage->packetData["options"][]=$this->handleIANA($option, $clientID, STATUS_NO_BINDING);	
			}				
		}

		$responsePackage->packetData["options"][]=array
			(
				"type"=>OPT_TYPE_STATUS,
				"len"=>strlen(STATUS_SUCCESS)/2,
				"data"=>STATUS_SUCCESS
			);


		$response = $this->buildResponse($responsePackage);
	}


	function handleRelayedMessage($option)
	{
		$response=array();
		$response["type"]=OPT_TYPE_RELAY_MSG;
		
		$packet = new dhcpv6Packet();
		$packet->parse(pack("H*",$option["data"]));
		$processor= new dhcpv6RequestProcessor($this->dhcpv6Server, $this->storage, $packet);
	

		$rawResponse=$processor->getResponse();
		if ($rawResponse != null)
		{
			$unpackedResponse=unpack("H*Resp",$rawResponse);			
			$response["data"]=$unpackedResponse["Resp"];
			$response["len"]=strlen($response["data"])/2;
		}
		else 
			$response = null;		
		return $response; 
	}


	function handleRelay($packet, &$response)
	{
		$responsePackage = new dhcpv6Packet;
		$responsePackage->packetData["type"]=MSG_TYPE_RELAY_REPL;
		$responsePackage->packetData["hop"]=0;
		$responsePackage->packetData["client"]=$packet->packetData["client"];
		$responsePackage->packetData["source"]=$packet->packetData["source"];
		$responsePackage->packetData["options"]=array();
		
		foreach($packet->packetData["options"] as $option)
		{
			if ($option["type"] == OPT_TYPE_RELAY_MSG)
			{
				$relayOption=$this->handleRelayedMessage($option);
				if ($relayOption != null)
					$responsePackage->packetData["options"][]=$relayOption;
				else
				{
					$response=null;
					return null;
				}
			}
			else if ($option["type"] == OPT_TYPE_INTERFACE_ID)
			{
				$responsePackage->packetData["options"][]=$option;
			}
		}
				
		$response=$this->buildRelayResponse($responsePackage);
	}


	function __construct(dhcpv6Server $server, $storage, $packet)
	{
		$this->dhcpv6Server = $server;
		$this->storage = $storage;
		$this->packet = $packet;
		$response = new dhcpv6Packet;
		
		switch($packet->packetData["type"])
		{
			case MSG_TYPE_SOLICIT:
				$this->handleClientSolicit($this->packet, $this->response);
				break;
			case MSG_TYPE_REQUEST:
				$this->handleClientRequest($this->packet, $this->response);
				break;
			case MSG_TYPE_CONFIRM:
				$this->handleClientConfirm($this->packet, $this->response);
				break;
			case MSG_TYPE_RENEW:
				$this->handleClientRenew($this->packet, $this->response);
				break;
			case MSG_TYPE_REBIND:
				$this->handleClientRebind($this->packet, $this->response);
				break;
			case MSG_TYPE_INFORMATION_REQUEST:
				$this->handleClientInfoRequest($this->packet, $this->response);
				break;
			case MSG_TYPE_RELEASE:
				$this->handleClientRelease($this->packet, $this->response);
				break;
			case MSG_TYPE_DECLINE:
				$this->handleClientDecline($this->packet, $this->response);
				break;
			case MSG_TYPE_RELAY_FORW:
				$this->handleRelay($this->packet, $this->response);
				break;
			default:
				break;
		}
	}
	
	function getResponse() {
		if ($this->response) {
			return $this->response;
		} else {
			return null;
		}
	}
}
