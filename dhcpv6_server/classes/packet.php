<?php

class dhcpv6Packet {
	public $packetData = array(
		"type" => 0,
		"len" => 0,
		"data" => "",
		"client" => "",
		"source" => "",
		"hop" => 0
	);
	
	function parseOptions($options){
		$arrOptions=array();		
		while(strlen($options)>0)
		{
			$pos=0;
			$len=
			$option=array();
			$option["type"]=hexdec(substr($options,0,4));
			$option["len"]=hexdec(substr($options,4,4));
			$option["data"]=substr($options,8,2*$option["len"]);
			$options=substr($options,8+2*$option["len"]);
			$arrOptions[]=$option;
		}
		return $arrOptions;
	}

	function parse($binaryData) {
		$packetArray=unpack("H2type/H*message",$binaryData);
		$type=hexdec($packetArray["type"]);
		if ($type != MSG_TYPE_RELAY_FORW)
		{
			$this->packetData=unpack("H2type/H6transaction/H*optionsBinary",$binaryData);
			$this->packetData["type"]=hexdec($this->packetData["type"]);
			$this->packetData["options"]=$this->parseOptions($this->packetData["optionsBinary"]);
		}
		else
		{
			$this->packetData=unpack("H2type/H2hop/H32client/H32source/H*optionsBinary",$binaryData);
			$this->packetData["type"]=hexdec($this->packetData["type"]);
			$this->packetData["hop"]=hexdec($this->packetData["hop"]);
			$this->packetData["client"]=$this->packetData["client"];
			$this->packetData["source"]=$this->packetData["source"];
			$this->packetData["options"]=$this->parseOptions($this->packetData["optionsBinary"]);
		}
	}
}

