<?php

ini_set("html_errors", "Off");
ini_set("display_errors", "On");

header("Content-Type: text/plain;charset=utf-8");
ini_set("expose_php", "Off");
header_remove("X-Powered-By");

const DATE_ISO8601_ZULU="Y-m-d\TH:i:s\Z";
date_default_timezone_set("UTC");
setlocale(LC_ALL, "en.utf8");

set_error_handler(function($nSeverity, $strMessage, $strFilePath, $nLineNumber){
	//error_log(PHP_EOL.date("Y-m-d H:m:s", time())." ".$strFilePath."#".$nLineNumber.": ".$strMessage.PHP_EOL);
	throw new ErrorException($strMessage, /*nExceptionCode*/ 0, $nSeverity, $strFilePath, $nLineNumber);
}, -1);
error_reporting(-1);
assert_options(ASSERT_ACTIVE, true);
assert_options(ASSERT_WARNING, true);
assert_options(ASSERT_QUIET_EVAL, false);


ignore_user_abort(true);


if((int)ini_get("short_open_tag")===1)
	throw new Exception("php.ini short_open_tag is active (short_open_tag=".var_export(ini_get("short_open_tag"), true).") and it will cause syntax errors.");

if(!defined("SIGILL"))
	define("SIGILL", 0);

ini_set("memory_limit", "512M");
