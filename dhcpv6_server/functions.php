<?php

require_once("classes/server.php");

function dhcpv6_boot_service_listen($nDHCPv6Port=547)
{
	$hOutput=fopen("php://stdout", "w+");
	
	dhcpv6_print_message("Starting DHCPv6 server", $hOutput);

	try
	{
		

		ignore_user_abort(true);
		try
		{
			$objDHCPv6Server = new dhcpv6Server($nDHCPv6Port);
			dhcpv6_print_message("listening on ".$nDHCPv6Port."\n", $hOutput);
			$objDHCPv6Server->listen();
		}
		catch(Exception	$e)
		{
			if (php_sapi_name()=="cli")
			{
				dhcpv6_print_message($e->getMessage());
				dhcpv6_print_message($e->getTraceAsString());
			}
			else
				throw $e;
		}
	}
	catch(Exception $exc)
	{
		$exception=$exc;
	}

	if(isset($exception))
		throw $exception;
}


