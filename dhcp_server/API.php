<?php
require_once("classes/dhcpserver.php");
require_once("dhcp_functions.php");

/**
* Starts a DHCP listening server. This function may be called via scripting via CLI, so the return is not important.
* Error handling depends on the php_sapi_name().
* For CLI, errors and status are displayed via php://stdout (and may be logged).
* For HTTP (HTTP JSONRPC processes), errors will be normal JSONRPC errors (Exceptions) and status and progress information will be available only on machine logs. If making a persistent API call, as long as the call is hanging, the process is up.
* This function will call ignore_user_abort(true), so it is safe to start the listening server process using JSONRPC notifications (see JSONRPC 2.0 spec).
* @param  int $nDHCPPort = 67.
* @return null. The return of this function doesn't matter.
*/
function dhcp_boot_service_listen($nDHCPPort=67)
{
	$hOutput=fopen("php://stdout", "w+");
	
	ignore_user_abort(true);
	try
	{
		$objDHCPServer = new dhcpServer($nDHCPPort);
		dhcp_print_message("listening on ".$nDHCPPort."\n",$hOutput);
		$objDHCPServer->listen();
	}
	catch(Exception	$e)
	{
		if (php_sapi_name()=="cli")
		{
			dhcp_print_message($e->getMessage());
			dhcp_print_message($e->getTraceAsString());
		}
		else
			throw $e;
	}
}

function dhcp_packet_process($objDHCPPacket)
{	
	$objDHCPResponse = dhcp_response_generate($objDHCPPacket);

	if ($objDHCPResponse == null)
		$objDHCPResponse = array();

	return $objDHCPResponse;
}

function dhcp_response_callback($arrDHCPResponse, $objDHCPPacket, $objSocket, $bVerbosity = true)
{
	if(is_object($arrDHCPResponse) && (get_class($arrDHCPResponse) === "Exception" || is_subclass_of($arrDHCPResponse, "Exception")))
	{
		dhcp_print_message($arrDHCPResponse->getMessage());
	}
	else if (count($arrDHCPResponse)>0)
	{
		$objDHCPResponse = new DHCPPacket;

		foreach($arrDHCPResponse["arrPacketData"] as $key => $value)
			$objDHCPResponse->setData($key, $value);

		$strResponseData = $objDHCPResponse->buildBinary();
	
		$bVerbosity && dhcp_print_message("Sending response");

		$strClientAddress = $objDHCPPacket->getClientAddress();
		if($strClientAddress == "0.0.0.0")
		{
			$strClientAddress = "192.168.240.255";
			$bVerbosity && dhcp_print_message("Switching to broadcast address...");
		}

		$strRelayAddress = $objDHCPPacket->getRelayAddress();
		if ($strRelayAddress != "0.0.0.0")
		{
			$strClientAddress = $strRelayAddress;
			$bVerbosity && dhcp_print_message("Attempting to send response package to " . $strClientAddress);
			$nNumBytesSent = socket_sendto($objSocket, $strResponseData, strlen($strResponseData), 0, $strClientAddress, 67);
		}
		else
		{
			$bVerbosity && dhcp_print_message("Attempting to send response package to " . $strClientAddress);
			$nNumBytesSent = socket_sendto($objSocket, $strResponseData, strlen($strResponseData), 0, $strClientAddress, 68);
		}

		if ($nNumBytesSent == FALSE)
		{
			$bVerbosity && dhcp_print_message("Send failed, trying broadcast");
			$nNumBytesSent = socket_sendto($objSocket, $strResponseData, strlen($strResponseData), 0, "192.168.240.255", 68);
			$nNumBytesSent && $bVerbosity && dhcp_print_message("Socket send error: " . socket_strerror(socket_last_error($objSocket)));
		}
		$nNumBytesSent && $bVerbosity && dhcp_print_message("Response packet sent.");
	}
	else
	{
		$bVerbosity && dhcp_print_message("Packet ignored");
	}
}
