<?php

function dhcp_information_for_client_get($strMACAddress, $arrRelayOptions, $bPXERequest)
{
	$arrIP = array(
		"node_interface_index" => 1,
		"ip_human_readable" => "192.168.240.129",
		"server_interface_mac_address" => $strMACAddress,
		"ippool_netmask_human_readable" => "255.255.255.0",
		"ippool_gateway_human_readable" => "192.168.240.132",
		"ippool_id" => 1,
		"ippool_destination" => "san"
	);

	return $arrIP;
}

function dhcp_response_generate($arrDHCPPacket)
{
	$objDHCPPacket = new DHCPPacket;

	foreach($arrDHCPPacket["arrPacketData"] as $key => $value)
		$objDHCPPacket->setData($key, $value);

	$objDHCPResponse = new DHCPPacket;	
	
	switch($objDHCPPacket->getMessageType())
	{
		case "discover":
			$objDHCPResponse = dhcp_response_type_generate($objDHCPPacket, "offer");
			break;
		case "request":
			$objDHCPResponse = dhcp_response_type_generate($objDHCPPacket, "ack");
			break;
		case "inform":
			$objDHCPResponse = dhcp_response_type_generate($objDHCPPacket, "ack");
			break;
		default;
			$objDHCPResponse = null;
			break;	
	}

	if (is_null($objDHCPResponse) || (!is_null($objDHCPResponse) && $objDHCPResponse->getClientAllocatedAddress() == "0.0.0.0"))
	{
		$objDHCPResponse = null;
	}

	return $objDHCPResponse;
}


function dhcp_response_type_generate($objDHCPPacket, $strResponseType)
{
	if ($objDHCPPacket->isPXERequest())
	{
		$strClientClassIdentification = $objDHCPPacket->getData("user_class_identification");

		if ($strClientClassIdentification=="iPXE")
			return dhcp_ipxe_response_generate($objDHCPPacket, $strResponseType);
		else
			return dhcp_generic_pxe_response_generate($objDHCPPacket, $strResponseType);
	}
	else
		return dhcp_generic_response_generate($objDHCPPacket, $strResponseType);
}


function dhcp_generic_response_generate($objDHCPPacket, $strResponseType)
{
	$objDHCPResponse = dhcp_ip_header_set($objDHCPPacket, $strResponseType);
	
	if (!is_null($objDHCPResponse))
	{

		$strRelayOptions = $objDHCPPacket->getData("relay_options");
		if ($strRelayOptions != "")
			$objDHCPResponse->setData("relay_options", $strRelayOptions);
	}

	return $objDHCPResponse;	
}


function dhcp_generic_pxe_response_generate($objDHCPPacket, $strResponseType)
{
	$strFileName = DHCPPacket::encodeAsFixedLenNullTerminatedString("undionly.kpxe", DHCPPacket::$arrFieldSizes["file"]);

	$objDHCPResponse = dhcp_ip_header_set($objDHCPPacket, $strResponseType);

	if(!is_null($objDHCPResponse))
	{
		$objDHCPResponse->setData("siaddr", DHCPPacket::ip2hex(explode(".", "192.168.240.132")));
		$objDHCPResponse->setData("file", $strFileName);

		$strRelayOptions = $objDHCPPacket->getData("relay_options");
		if ($strRelayOptions != "")
			$objDHCPResponse->setData("relay_options", $strRelayOptions);
	}

	return $objDHCPResponse;
}

function dhcp_ipxe_response_generate($objDHCPPacket, $strResponseType)
{
	$objDHCPResponse = dhcp_ip_header_set($objDHCPPacket, $strResponseType);

	if (!is_null($objDHCPResponse))
	{
		$objDHCPResponse->setData("siaddr", DHCPPacket::ip2hex(explode(".", "192.168.240.132")));
		$objDHCPResponse->setData("file", DHCPPacket::encodeAsFixedLenNullTerminatedString("nofile", DHCPPacket::$arrFieldSizes["file"]));

		$strRelayOptions = $objDHCPPacket->getData("relay_options");
		if ($strRelayOptions != "")
			$objDHCPResponse->setData("relay_options", $strRelayOptions);
	}

	return $objDHCPResponse;
}

function dhcp_ip_header_set($objDHCPPacket, $strResponseType)
{
	$objDHCPResponse = new DHCPPacket;

	$objDHCPResponse->setData("op", DHCPPacket::int2hex(2));
	$objDHCPResponse->setData("xid", $objDHCPPacket->getData("xid"));
	$objDHCPResponse->setData("chaddr", $objDHCPPacket->getData("chaddr"));
	$objDHCPResponse->setData("message_type", $strResponseType);
	$objDHCPResponse->setData("lease_time", 3600);

	$strMACAddress = $objDHCPPacket->getMACAddress();
	$arrRelayOptions = $objDHCPPacket->getRelayOptions();

	$arrClientInformation = dhcp_information_for_client_get($strMACAddress, $arrRelayOptions, $objDHCPPacket->isPXERequest());

	if (is_null($arrClientInformation))
	{
		$objDHCPResponse = null;
	}
	else
	{

		$arrRequiredFields=array(
			"ip_human_readable",
			"ippool_gateway_human_readable",
			"ippool_netmask_human_readable"
		);

		foreach($arrRequiredFields as $strFieldName)
		{
			if (!isset($arrClientInformation[$strFieldName]))
				throw new Exception("Need to have set ". $strFieldName ." in change set to allocate an IP");
		}

		//dhcp_print_message(var_export(explode("." ,$arrClientInformation["ip_human_readable"]), true));

		if ($objDHCPPacket->getData("giaddr") != "00000000")
			$objDHCPResponse->setData("giaddr", $objDHCPPacket->getData("giaddr"));

		$objDHCPResponse->setData("yiaddr", DHCPPacket::ip2hex(explode(".", $arrClientInformation["ip_human_readable"])));
		$objDHCPResponse->setData("siaddr", DHCPPacket::ip2hex(explode(".", $arrClientInformation["ippool_gateway_human_readable"])));
		$objDHCPResponse->setData("subnet_mask", explode(".", $arrClientInformation["ippool_netmask_human_readable"]));
		$objDHCPResponse->setData("router", explode(".", $arrClientInformation["ippool_gateway_human_readable"]));
		$objDHCPResponse->setData("server_id", explode(".", $arrClientInformation["ippool_gateway_human_readable"]));

		if (array_key_exists("ippool_static_route_prefix", $arrClientInformation))
		{
			$arrGatewayIP = explode(".", $arrClientInformation["ippool_gateway_human_readable"]);
			$arrPrefix = explode(".", $arrClientInformation["ippool_static_route_prefix"]);
			$nNetmask = (int)$arrClientInformation["ippool_static_route_netmask"];
			$nByteCount = (int)($nNetmask/8);

			$arrSmallerPrefix = array_slice($arrPrefix, 0, $nByteCount);
		
			$arrDestinationDescriptor = array_merge(
				array($nNetmask),
				$arrSmallerPrefix,
				$arrGatewayIP
			);

			for($nIterator=0; $nIterator < count($arrDestinationDescriptor); $nIterator++)
				$arrDestinationDescriptor[$nIterator]=chr($arrDestinationDescriptor[$nIterator]);

			$strDestinationDescriptor=implode("", $arrDestinationDescriptor);
			
			$objDHCPResponse->setData("classless-static-route", DHCPPacket::string2hex($strDestinationDescriptor));
		}

	}

	return $objDHCPResponse;
}
