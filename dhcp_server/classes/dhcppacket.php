<?php

class DHCPPacket{
	const BOOTREPLY = "02";
	const BOOTREQUEST = "01";

	public static $arrOptions = array(
		1 => array("name" => "subnet_mask", "type" => "ip"),
		2 => array("name" => "time_offset", "type" => "int"),
		3 => array("name" => "router", "type" => "ip"),
		6 => array("name" => "dns_server", "type" => "ip"),
		12 => array("name" => "host_name", "type" => "string"),
		15 => array("name" => "domain_name", "type" => "string"),
		28 => array("name" => "broadcast_address", "type" => "ip"),
		50 => array("name" => "requested_ip_address", "type" => "ip"),
		51 => array("name" => "lease_time", "type" => "int"),
		53 => array("name" => "message_type", "type" => "messageType"),
		54 => array("name" => "server_id", "type" => "ip"),
		55 => array("name" => "parameter_request", "type" => "binary"),
		57 => array("name" => "max_message_size", "type" => "int"),
		58 => array("name" => "renewal_time", "type" => "int"),
		59 => array("name" => "rebinding_time", "type" => "int"),
		61 => array("name" => "client_id", "type" => "mac"),
		60 => array("name" => "vendor_class_identifier", "type" => "string"),
		66 => array("name" => "tftp_server", "type" => "string"),
		67 => array("name" => "filename", "type" => "string"),

 77 => array("name" => "user_class_identification", "type" => "string"),
		91 => array("name" => "client-last-transaction-time", "type" => "int"),
		92 => array("name" => "associated-ip", "type" => "string"),

		/*PXE specific options RFC4578 */
		93 => array("name" => "client_system_type", "type" => "binary"),
		94 => array("name" => "client_network_interface_identifier", "type" => "binary"),
		97 => array("name" => "uuid", "type" => "binary"),

		/*iPXE specific options */
		175 => array("name" => "ipxe-encapsulated-options","type"=>"binary"),
		121 => array("name" => "classless-static-route", "type" => "binary"),
		33 => array("name" => "static-route", "type" => "string"),
		82 => array("name" => "relay_options", "type" => "string")
	);

	public static $arrMessageTypes = array(
		1 => "discover",
		2 => "offer",
		3 => "request",
		4 => "decline",
		5 => "ack",
		6 => "nak",
		7 => "release",
		8 => "inform",
		10 => "DHCPLEASEQUERY",
		11 => "DHCPLEASEUNASSIGNED",
		12 => "DHCPLEASEUNKNOWN",
		13 => "DHCPLEASEACTIVE"
	);

	public $arrPacketData = array(
		"op" => "02",
		"htype" => "01",
		"hlen" => "06",
		"hops" => "00",
		"xid" => "00000000",
		"secs" => "0000",
		"flags" => "0000",
		"ciaddr" => "00000000",
		"yiaddr" => "00000000",
		"siaddr" => "00000000",
		"giaddr" => "00000000",
		"chaddr" => "00000000000000000000000000000000",
		"sname" => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
		"file" => "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
		"magic" => "63825363"
	);

	public static $arrFieldSizes=array(
		"chaddr"=>16,
		"sname"=>64,
		"file"=>128
	);

	function setData($strKey, $newValue)
	{
		$this->arrPacketData[$strKey]=$newValue;
	}	

	function getData($strKey)
	{
		return isset($this->arrPacketData[$strKey])?$this->arrPacketData[$strKey]:"";
	}

	function getMessageType()
	{
		return $this->arrPacketData["message_type"];	
	}

	function getRequestedParams()
	{
		$strRequestedParams = $this->getData("parameter_request");
		$arrRequestedParams = str_split($strRequestedParams, 2);
		$arrRequestedOptions = array();
		foreach($arrRequestedParams as $strHexOptionNumber)
		{
			$nOptionNumber=dhcppacket::hex2int($strHexOptionNumber);
			if (isset(dhcppacket::$arrOptions[$nOptionNumber]))
				$arrRequestedOptions[]=dhcppacket::$arrOptions[$nOptionNumber]["name"];
		}

		return $arrRequestedOptions;
	}
	
	function getClientAddress()
	{
		$strClientAddress = $this->getData("ciaddr");
		$arrClientAddress = self::hex2ip($strClientAddress);
		return implode(".", $arrClientAddress);
	}

	function getClientAllocatedAddress()
	{
		$strClientAddress = $this->getData("yiaddr");
		$arrClientAddress = self::hex2ip($strClientAddress);
		return implode(".", $arrClientAddress);
	}

	function getRelayAddress() 
	{
		$strRelayAddress = $this->getData("giaddr");
		$arrRelayAddress = self::hex2ip($strRelayAddress);
		return implode(".", $arrRelayAddress);
	}


	function getMACAddress()
	{
		$strMACAddress = $this->getData("chaddr");
		$arrMACAddress = str_split(substr($strMACAddress, 0, 12), 2);
		return implode(":", $arrMACAddress); 
	}	

	function getRelayOptions()
	{
		if (isset($this->arrPacketData["relay_options"]))
		{
			$strRelayOptions=$this->arrPacketData["relay_options"];
			$arrSplitted=str_split($strRelayOptions,1);
			$arrRelayOptions=array();

			for($nOptionsIterator=0;$nOptionsIterator<count($arrSplitted);$nOptionsIterator=$nOptionsIterator+1)
			{
				$arrRelaySuboption=array();
				$arrRelaySuboption["type"] = ord($arrSplitted[$nOptionsIterator]);
				$arrRelaySuboption["length"] = ord($arrSplitted[$nOptionsIterator+1]);
				$arrRelaySuboption["content"] = implode("", array_slice($arrSplitted, $nOptionsIterator+2, $arrRelaySuboption["length"]));
				$arrRelayOptions[]=$arrRelaySuboption;

				$nOptionsIterator=$nOptionsIterator+$arrRelaySuboption["length"]+1;
			}

			return $arrRelayOptions;
		}
		else
			return null;
	}

	function getUUID()
	{
		return isset($this->arrPacketData["uuid"])?self::hex2uuid($this->arrPacketData["uuid"]):null;
	}	

	function isPXERequest()
	{
		$arrPXERequiredOptions=array(
			"client_system_type",
			"client_network_interface_identifier",
			"uuid"
		);

		foreach($arrPXERequiredOptions as $strPXEOption)
			if (!isset($this->arrPacketData[$strPXEOption]))
				return false;

		return true;
	}

	function parseBinary($strBinaryData)
	{
		$this->arrPacketData = unpack("H2op/H2htype/H2hlen/H2hops/H8xid/H4secs/H4flags/H8ciaddr/H8yiaddr/H8siaddr/H8giaddr/H32chaddr/H128sname/H256file/H8magic/H*options", $strBinaryData);
		$strOptionData = $this->arrPacketData["options"];
		
		$nOptionIterator = 0;
	
		while (strlen($strOptionData) > $nOptionIterator)
		{
			$nOptionCode = base_convert(substr($strOptionData, $nOptionIterator, 2), 16, 10);
			$nOptionIterator += 2;
			$nOptionLength = base_convert(substr($strOptionData, $nOptionIterator, 2), 16, 10);
			$nOptionIterator += 2;
			$strCurrentOption = substr($strOptionData, $nOptionIterator, $nOptionLength*2);
			$nOptionIterator += 2*$nOptionLength;

			if (isset(self::$arrOptions[$nOptionCode]))
				$arrOptionConfig = self::$arrOptions[$nOptionCode];
			else
				$arrOptionConfig = null;

			if ($arrOptionConfig)
			{
				$strCurrentOptionData = null;
				
				switch($arrOptionConfig["type"])
				{
					case "int":
						$strCurrentOptionData = base_convert($strCurrentOption, 16, 10);
						break;
					case "string":
						$strCurrentOptionData = self::hex2string($strCurrentOption);
						break;
					case "messageType":
						$strCurrentOptionData = self::$arrMessageTypes[self::hex2int($strCurrentOption)];
						break;
					default:
						$strCurrentOptionData = $strCurrentOption;
						break;
				}

				$this->arrPacketData[$arrOptionConfig["name"]] = $strCurrentOptionData;	
			}
			else
				$this->arrPacketData[$nOptionCode] = $strCurrentOption;
		}
	}

	function buildBinary()
	{
		$arrPacketData = $this->arrPacketData;
		$strOptions = "";

		foreach(self::$arrOptions as $nOptionCode => $arrOptionConfig)
			if (isset($this->arrPacketData[$arrOptionConfig["name"]]))
			{
				$strOptionData = $this->arrPacketData[$arrOptionConfig["name"]];
				$strOptionCode = self::int2hex($nOptionCode);

				switch($arrOptionConfig["type"])
				{
					case "int":
						$strOptionHex = self::int2hex($strOptionData);
						break;
					case "string":
						$strOptionHex = self::string2hex($strOptionData);
						break;
					case "ip":
						$strOptionHex = self::ip2hex($strOptionData);
						break;
					case "messageType":
						$strOptionHex = self::int2hex(array_search($strOptionData, self::$arrMessageTypes));
						break;
					default:
						$strOptionHex = $strOptionData;
						break;
				}

				$strOptions.= $strOptionCode . self::int2hex(strlen($strOptionHex)/2) . $strOptionHex;
			}
		
		$strOptions .= self::int2hex(255) . "00";

		$strBinaryData = pack("H2H2H2H2H8H4H4H8H8H8H8H32H128H256H8H*",
			$arrPacketData["op"],
			$arrPacketData["htype"],
			$arrPacketData["hlen"],
			$arrPacketData["hops"],
			$arrPacketData["xid"],
			$arrPacketData["secs"],
			$arrPacketData["flags"],
			$arrPacketData["ciaddr"],
			$arrPacketData["yiaddr"],
			$arrPacketData["siaddr"],
			$arrPacketData["giaddr"],
			$arrPacketData["chaddr"],
			$arrPacketData["sname"],
			$arrPacketData["file"],
			$arrPacketData["magic"],
			$strOptions
		);

		return $strBinaryData;
	}

	public static function ip2hex($arrIP) 
	{
		return self::int2hex($arrIP[0]) .
			self::int2hex($arrIP[1]) .
			self::int2hex($arrIP[2]) .
			self::int2hex($arrIP[3]);
	}

	public static function hex2ip($strHexIP) 
	{
		return array(
			self::hex2int($strHexIP[0] . $strHexIP[1]),
			self::hex2int($strHexIP[2] . $strHexIP[3]),
			self::hex2int($strHexIP[4] . $strHexIP[5]),
			self::hex2int($strHexIP[6] . $strHexIP[7])
		);
	}

	public static function string2hex($strData) 
	{
		$strHexData = "";
		for ($nStringIterator = 0; $nStringIterator < strlen($strData); $nStringIterator++) {
			$strCurrent = dechex(ord($strData[$nStringIterator]));
			if (strlen($strCurrent) == 1)
				$strCurrent = "0".$strCurrent;
			$strHexData .= $strCurrent;
		}
		return $strHexData;
	}

	public static function hex2string($strHexData) 
	{
		$strData = "";
		for ($nHexIterator = 0; $nHexIterator < strlen($strHexData) - 1; $nHexIterator += 2) {
			$strData .= chr(hexdec($strHexData[$nHexIterator] . $strHexData[$nHexIterator + 1]));
		}
		return $strData;
	}

	public static function hex2int($strHexData) 
	{
		return base_convert($strHexData, 16, 10);
	}

	public static function int2hex($nToConvert) {
		$strHexData = base_convert($nToConvert, 10, 16);
		// base_convert doesn't do the necesarry padding
		if (strlen($strHexData) == 1) {
			$strHexData = "0" . $strHexData;
		} else if (strlen($strHexData) == 3) {
			$strHexData = "0" . $strHexData;
		} else if (strlen($strHexData) == 5) {
			$strHexData = "000" . $strHexData;
		} else if (strlen($strHexData) == 7) {
			$strHexData = "0" . $strHexData;
		}
		return "" . $strHexData;
	}

	public static function uuid2hex($strUUID)
	{
		return str_replace("-", "", $strUUID);
	}

	public static function hex2uuid($strHexUUID)
	{
		//IPXE bug: instead of 16 byte, it send a 17 byte UUID, being padded with 00 at the beginning. 
		$strHexUUID=substr($strHexUUID,2);
		assert(strlen($strHexUUID)==32);
		//UUID format is: 8-4-4-4-12

		return
			substr($strHexUUID,0,8).
			"-".substr($strHexUUID,8,4).
			"-".substr($strHexUUID,12,4).
			"-".substr($strHexUUID,16,4).
			"-".substr($strHexUUID,20);
	}

	public static function encodeAsFixedLenNullTerminatedString($strPrefix, $nSize=128)
	{
		$strHexData=DHCPPacket::string2hex($strPrefix);
		$nLength=$nSize*2-strlen($strHexData);
		for($nPaddingIterator=0;$nPaddingIterator<$nLength;$nPaddingIterator++)
			$strHexData.="0";
		return $strHexData;
	}	
}
