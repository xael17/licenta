<?php
/*
  Copyright 2009 Angelo R. DiNardi (angelo@dinardi.name)
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
	http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

require_once "dhcppacket.php";

class DHCPServer 
{
	private $socket = null;
	public $verbosity;// 0-2 - level of verbosity (0 to sqelch, 1 for a little, 2 for a lot (packet dumps))
	private $broadcast_addr;
	private $_port;
	const DHCP_CONFIG_POLL_TIMEOUT = 200;
	const DHCP_CONFIG_READ_TIMEOUT = 200;
	const DHCP_CONFIG_MAX_WORKERS = 3;
	
	function __construct($port=67, $verbosity = 1)
	{
		$this->verbosity = $verbosity;
		$this->broadcast_addr = "192.168.52.255";
		$this->_port=$port;
		$this->socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
		socket_bind($this->socket, "0.0.0.0", $this->_port);
		socket_set_option($this->socket, SOL_SOCKET, SO_BROADCAST, 1);
		socket_set_option($this->socket, SOL_SOCKET, SO_REUSEADDR, 1);
	}

	function getSocketHost()
	{
		return socket_getsockname($this->socket, $addr) ? $addr : NULL;  
	}

	function listen() 
	{
		while(true)
		{


			$strPacketData = socket_read($this->socket, 4608);

			dhcp_print_message("Received new message!");

			$objDHCPPacket = new DHCPPacket;
			$objDHCPPacket->parseBinary($strPacketData);

			$objDHCPResponse = dhcp_packet_process(json_decode(json_encode($objDHCPPacket), true));

			dhcp_response_callback(json_decode(json_encode($objDHCPResponse), true), $objDHCPPacket, $this->socket);
		}

	}
}


function dhcp_print_message($strMessage, $hOutput=null)
{

	static $hOutputStream;
	if($hOutput!=null)
		$hOutputStream = $hOutput;
	if(!is_null($hOutputStream))
		fwrite($hOutputStream, $strMessage."\n");
	else
		echo $strMessage."\n";
}

