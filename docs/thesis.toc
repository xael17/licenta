\select@language {romanian}
\contentsline {chapter}{\numberline {1}Introducere}{3}{chapter.1}
\contentsline {chapter}{\numberline {2}Tehnologii de ultim\IeC {\u a} or\IeC {\u a}}{5}{chapter.2}
\contentsline {chapter}{\numberline {3}Arhitectura}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Entit\IeC {\u a}\IeC {\c t}i}{9}{section.3.1}
\contentsline {section}{\numberline {3.2}Procesul de bootare IPv4}{12}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Etapa de bootstraping}{12}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Etapa de ob\IeC {{\textcommabelow t}}inere a creden\IeC {{\textcommabelow t}}ialelor \IeC {{\textcommabelow s}}i de contactare a serverului de date}{14}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Procesul de bootare IPv6 - Argus6}{16}{section.3.3}
\contentsline {chapter}{\numberline {4}Implementare}{19}{chapter.4}
\contentsline {section}{\numberline {4.1}Mediu de testare}{20}{section.4.1}
\contentsline {section}{\numberline {4.2}Server DHCP (bootstrapping)}{21}{section.4.2}
\contentsline {section}{\numberline {4.3}Server DHCPv6}{22}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Transla\IeC {\c t}ie DHCPv4-DHCPv6}{24}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Client identifier}{24}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}Undionly.kpxe}{25}{section.4.4}
\contentsline {section}{\numberline {4.5}Server iSCSI}{27}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}iSCSI URI}{28}{subsection.4.5.1}
\contentsline {section}{\numberline {4.6}Server web}{28}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Script iPXE}{29}{subsection.4.6.1}
\contentsline {section}{\numberline {4.7}Sisteme de operare}{30}{section.4.7}
\contentsline {subsection}{\numberline {4.7.1}Debian}{30}{subsection.4.7.1}
\contentsline {subsection}{\numberline {4.7.2}CentOS}{30}{subsection.4.7.2}
\contentsline {chapter}{\numberline {5}Rezultate}{32}{chapter.5}
\contentsline {section}{\numberline {5.1}Func\IeC {\c t}ionarea aplica\IeC {\c t}iei}{32}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Bootstrap}{33}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}DHCPv6}{34}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}iSCSI}{36}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Sistem de operare}{37}{subsection.5.1.4}
\contentsline {section}{\numberline {5.2}Imbun\IeC {\u a}t\IeC {\u a}\IeC {\c t}iri viitoare}{38}{section.5.2}
\contentsline {chapter}{\numberline {6}Concluzii}{40}{chapter.6}
\contentsline {chapter}{Bibliografie}{41}{chapter.6}
